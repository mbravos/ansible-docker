#! /bin/bash
#
# run-local.sh
#

ansible-playbook playbook-docker.yml -i "localhost," -c local --ask-become-pass -vvvvv
